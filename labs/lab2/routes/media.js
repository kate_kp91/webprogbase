var express = require('express');
var router = express.Router();
var mediaController = require('../controllers/mediaController');



/**
 * GET /api/media/{media_id}     -> {Media}. 
 * getting image by id
 * @route GET /api/media/{id}
 * @group Media - media operations
 * @param {integer} id.path.required - id of the media file - eg: 1
 * @returns {Media.model} 200 - Media object
 * @returns {Error} 404 - Media not found
 */
router.get('/api/media/:id', mediaController.getMediaById);


/**
 * GET /api/media     -> [{Media},{Media}].
 * Getting all medias
 * @route GET /api/media
 * @group Media - media operations
* @param {integer} pageNumber.query - displayed page - eg:1
 * @param {integer} perPage.query - displayed count of media per page - eg:10
 * @returns {Array.<Media>} 200 - All medias
 */
router.get('/api/media', mediaController.getMedias);

/**
 * GET /api/mediaDisplay     -> [{Media},{Media}].
 * Displaying all medias
 * @route GET /api/mediaDisplay
 * @group Media - media operations
 * @param {integer} pageNumber.query - displayed page - eg:1
 * @param {integer} perPage.query - displayed count of media per page - eg:10
 * @returns {Array.<Media>} 200 - All medias
 */
router.get('/api/mediaDisplay', mediaController.getMediasDisplay);


/**
 * Adding new image
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {integer} 200 - added image id
 */
router.post('/api/media', mediaController.addMedia);


/**
 * Form for adding new image
 * @route GET /api/upload
 * @group Media - upload and get images
 * @returns {View} html form
 */
router.get('/api/upload', mediaController.upload);

module.exports = router;
