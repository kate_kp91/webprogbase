var express = require('express');
var router = express.Router();
var controller = require('../controllers/actorController');
var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()


// /* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

/**
 * GET /api/actors     -> [{Actor}, {Actor}].
 * getting list of actors
 * @route GET /api/actors
 * @group Actors - actor operations
 * @param {integer} pageNumber.query - displayed page - eg:1
 * @param {integer} perPage.query - displayed count of actors per page - eg:10
 * @returns {Array.<Actor>} 200 - Page with actors
 */
router.get('/api/actors', controller.getActors);

/**
 * GET /api/actors/:id -> {Actor}.
 * getting actor by id
 * @route GET /api/actors/{id}
 * @group Actors - Actor operations
 * @param {integer} id.path.required - id of the Actor - eg: 1
 * @returns {Actor.model} 200 - Actor object
 * @returns {Error} 404 - Actor not found
 */
router.get('/api/actors/:id', controller.getActor);

//POST   /api/{entities}     -> {Entity}               // return added entity */
/**
 * Creating new actor
 * @route POST /api/actors
 * @group Actors - actor operations
 * @param {Actor.model} id.body.required - new Actor object
 * @returns {Actor.model} 201 - added Actor object
 */
router.post('/api/actors', jsonParser, controller.createActor);

//PUT    /api/{entities}     -> {Entity}               // return changed entity
/**
 * Updating actor info
 * @route PUT /api/actors
 * @group Actors - actor operations
 * @param {Actor.model} id.body.required - new Actor object
 * @returns {Actor.model} 200 - changed Actor object
 * @returns {Error} 404 - Actor not found
 */
router.put('/api/actors', jsonParser, controller.updateActor);

//DELETE /api/{entities}/:id -> {Entity}               // return removed entity
/**
 * Deleting actor by id
 * @route DELETE /api/actors/{id}
 * @group Actors - actor operations
 * @param {integer} id.path.required - id of the Actor - eg: 1
 * @returns {Actor.model} 200 - deleted Actor object
 * @returns {Error} 404 - Actor not found
 */
router.delete('/api/actors/:id', controller.deleteActor);

module.exports = router;
