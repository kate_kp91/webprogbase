const Actor = require('../models/actor');
var actor = require('../models/actor');
const Repository = require('../repositories/actorRepository');
const repository = new Repository('./data/actors.json');

exports.getActors = function (req, res) {
  var actors = repository.getActors(req.query.pageNumber, req.query.perPage);

  if (actors.length > 0) {
    res.status(200).send({ actors, message: 'Success' });
  } else {
    res.status(200).send({ actors, message: 'There is no actors!'});
  }
};

exports.getActor = function (req, res) {
  var id = req.params.id;
  var actor = repository.getActorById(id);
  if (actor) {
    res.status(200).send({ actor, message: 'Success' });
  } else {
    res.status(404).send({ actor: null, message: 'Actor not found' });
  }
};

exports.createActor = function (req, res) {
  var actor = req.body;
  var actorModel = new Actor(null, actor.name, actor.surname, actor.age, actor.films, new Date().toISOString());
  
  repository.addActor(actorModel, (code, status, actor) => {
    res.status(code).send({ actor, message: status });
  });
};

exports.updateActor = function (req, res) {
  var actor = req.body;
  var updatedActor = repository.updateActor(new Actor(actor.id, actor.name, actor.surname, actor.age, actor.films, actor.registeredAt));
  if (updatedActor) {
    res.status(200).send({ updatedActor, message: 'Update is Successfull' });
  } else {
    res.status(404).send({ updatedActor: null, message: 'Not updated. Actor not found' });
  }
};

exports.deleteActor = function (req, res) {
  var actor = repository.deleteActor(req.params.id)
  if (actor) {
    res.status(200).send({ actor, message: 'Delet is successfull' });
  } else {
    res.status(404).send({ actor: null, message: 'Not deleted. Actor not found id:'  + req.params.id});
  }
};