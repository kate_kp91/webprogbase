const e = require('express');
const MediaRepository = require('../repositories/mediaRepository');
const mediaRepository = new MediaRepository('./data/media.json', './data/media');


exports.getMediaById = function (req, res) {
  var media = mediaRepository.getMediaById(req.params.id);
  if(media) {
    res.status(200).send({media, message:'Success'});
  } else {
    res.status(404).send({ media:null, message: 'Media not found' });
  }
};

exports.addMedia = function (req, res) {
  mediaRepository.addMedia(req, res, (code, status, id)=> {
    res.status(code).send({ mediaId: id, message: status});
  });
};

exports.getMedias = function (req, res) {
  var medias = mediaRepository.getMedias(req.query.pageNumber, req.query.perPage);
  if (medias.length > 0) {
    res.status(200).send({ medias, message: 'Success' });
  } else {
    res.status(200).send({ medias, message: 'There is no medias!' });
  }
};

exports.getMediasDisplay = function (req, res) {
 // res.render('medias', { images: mediaRepository.getMedias()});
  res.render('medias', { images: mediaRepository.getMedias(req.query.pageNumber, req.query.perPage), dirname : __dirname});
};

exports.upload = function (req, res) {
  res.render('upload', { title: 'Express' });
};