var user = require('../models/user');
const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');


exports.getUsers = function (req, res) {
  var users = userRepository.getUsers(req.query.pageNumber, req.query.perPage);
  if (users.length > 0) {
    res.status(200).send({ users, message: 'Success'});
  } else {
    res.status(200).send({ users, message: 'There is no users!'});
  }
};

exports.getUser = function (req, res) {
  var user = userRepository.getUserById(req.params.id);

  if (user) {
    res.status(200).send({ user, message: 'Success' });
  } else {
    res.status(404).send({ user: null, message: 'User not found id:' + req.params.id });
  }
};