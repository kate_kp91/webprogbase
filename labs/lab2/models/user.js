
// models/user.js

/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - full name of user
 * @property {integer} role - role of user
 * @property {string} registeredAt - time when user registered
 * @property {string} avaUrl - url address of user avatart
 * @property {boolean} isEnabled - flag that determines is user enabled or diactivated
 */

class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isEnabled = isEnabled ;
        
    }
};

module.exports = User;