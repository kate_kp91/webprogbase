
/**
 * @typedef Actor
 * @property {integer} id
 * @property {string} name.required - name of  actor
 * @property {string} surname.required - surname of actor
 * @property {integer} films - count of actor films
 * @property {integer} age.required - age of actor
 * @property {string} registeredAt - time when actor registered
*/
class Actor {

    constructor(id, name, surname, age, films, registeredAt) {
        this.id = id;  // number
        this.name = name;  // string
        this.surname = surname;  // string
        this.age = age;
        this.films = films;
        this.registeredAt = registeredAt;
        }
};

module.exports = Actor;
