
// models/Media.js

/**
 * @typedef Media
 * @property {integer} id
 * @property {string} filePath.required - filePath
*/

class Media {

    constructor(id, filePath) {
        this.id = id;  // number
        this.filePath = filePath;  // string
    }
};

module.exports = Media;