const express = require('express');
const routesUsers = require('./routes/users');
const routesActors = require('./routes/actors');
const routesMedia = require('./routes/media');
const app = express();
const port = 3000;

const morgan = require('morgan');
app.use(morgan('dev'));
app.set('view engine', 'jade');
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', routesUsers);
app.use('/', routesActors);
app.use('/', routesMedia);

app.use('/data/media/', express.static('./data/media'));

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);

const options = {
    swaggerDefinition: {
        info: {
            description: 'Creating HTTP API',
            title: 'Lab 2',
            version: '1.0.0',
        },
        host: 'localhost' + ':'+port,
        produces: ["application/json"],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);



app.listen(port, () => console.log('Gator app listening on port !' + port));