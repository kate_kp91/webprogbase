const Media = require('../models/media');
const JsonStorage = require('../jsonStorage');
const MediaStorage = require('../mediaStorage');
const formidable = require('formidable')

const perPage = 5;
const dirPath = 'data/media/';



class MediaRepository {

    constructor(filePath, downloadDir) {
        this.storage = new JsonStorage(filePath);
        this.mediaStorage = new MediaStorage(downloadDir);
    }

    getMediaById(id) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id == id) {
                return new Media(item.id, item.filePath);
            }
        }
        return null;

    }

    addMedia(req, res, next) {

        var mediaModel = new Media(null, null);
        new formidable.IncomingForm().parse(req)
            .on('fileBegin', (name, file) => {
                file.path = __dirname +'/../' + dirPath + file.name
            })
            .on('file', (name, file) => {
                var medias = this.getMedias();
                mediaModel.id = this.storage.nextId;
                mediaModel.filePath = dirPath + file.name;
                medias.push(mediaModel);

                this.storage.incrementNextId();
                this.storage.writeItems(medias);

                next(201, 'Created', mediaModel.id);
            })
            .on('aborted', () => {
                console.error('Request aborted by the user')
                next(400, 'Request aborted by the user');
            })
            .on('error', (err) => {
                console.error('Error', err)
                next(400, err);
                throw err
            })
    }

    getMedias(page, per_page) {

        per_page = per_page > 0 ? per_page : perPage;

        const items = this.storage.readItems();
        var medias = new Array();
        var itemsCount = items.length;

        var end = page > 0 && per_page > 0 ? page * per_page : itemsCount;
        var start = page > 0 && per_page > 0 ? end - per_page : 0;

        for (var i = start; (i < end && i < itemsCount); i++) {
            var item = items[i];
            medias.push(new Media(item.id, item.filePath));
        }
       
        return medias;
    }
}
module.exports = MediaRepository;
