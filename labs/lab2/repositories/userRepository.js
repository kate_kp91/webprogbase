const User = require('./../models/user');
const JsonStorage = require('../jsonStorage');

const perPage = 5;


class UserRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getUsers(page, per_page) {
        per_page =  per_page > 0 ? per_page : perPage;

        const items = this.storage.readItems();
        var users = new Array();
        var itemsCount = items.length;
        
        var end = page > 0 && per_page > 0 ? page * per_page : itemsCount;
        var start = page > 0 && per_page > 0 ? end - per_page : 0;
        
        for (var i = start; (i < end && i < itemsCount); i++) {
            
            var item = items[i];
            
            users.push(new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled));
           
        }
        return users;
    }

    getUserById(id) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id == id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;

    }

    addUser(userModel) {
        throw new Error("Not implemented");
    }

    updateUser(userModel) {
        throw new Error("Not implemented");
    }

    deleteUser(userId) {
        throw new Error("Not implemented");
    }

}
module.exports = UserRepository;
