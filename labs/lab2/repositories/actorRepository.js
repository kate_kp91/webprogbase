const Actor = require('./../models/actor');
const JsonStorage = require('../jsonStorage');
const e = require('express');

const perPage = 5;


class actorRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getActors(page, per_page) {
        per_page = per_page > 0 ? per_page : perPage;
        const items = this.storage.readItems();
        var actors = new Array();
        var itemsCount = items.length;

        var end = page > 0 && per_page > 0 ? page * per_page : itemsCount;
        var start = page > 0 && per_page > 0 ? end - per_page : 0;

        for (var i = start; (i < end && i < itemsCount); i++) {
            var item = items[i];
            actors.push(new Actor(item.id, item.name, item.surname, item.age, item.films, item.registeredAt));
        }
        return actors;
    }

    getActorById(id) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id == id) {
                return new Actor(item.id, item.name, item.surname, item.age, item.films, item.registeredAt);
            }
        }
        return null;

    }

    addActor(actorModel, next) {

        if(!actorModel.name) {
            next(400, 'Name field is missing', null);
            return;
        }
        
        if (!actorModel.surname) {
            next(400, 'Surname field is missing', null); 
            return;
        }
        
        if (actorModel.age != null) {
            next(400, 'Age field is missing', null);
            return;
        } 

        var actors = this.getActors();
        actorModel.id = this.storage.nextId;
        actors.push(actorModel);

        this.storage.incrementNextId();
        this.storage.writeItems(actors);


        next(201, 'Created successfully', actorModel);
    }

    updateActor(actorModel) {
        var actorId = actorModel.id;
        var actors = this.getActors();
        var updated = false;
        for (var i = 0; i < actors.length; i++) {
            if (actorId == actors[i].id) {

                if (actorModel.name) actors[i].name = actorModel.name;
                if (actorModel.surname) actors[i].surname = actorModel.surname;
                if (actorModel.age != null) actors[i].age = actorModel.age;
                if (actorModel.films != null) actors[i].films = actorModel.films;
                if (actorModel.registeredAt) actors[i].registeredAt = actorModel.registeredAt;
                updated = true;
                break;
            }    
        }

        if (updated) {
            this.storage.writeItems(actors);
            return this.getActorById(actorId);
        } else {
            return null;
        }
    }

    deleteActor(actorId) {
        var deletedActor = null;
        var actors = this.getActors();
        for (var i = 0; i < actors.length; i++) {
            if (actorId == actors[i].id) {
                deletedActor = actors[i];
                actors.splice(i, 1);
                break;
            }
        }

        this.storage.writeItems(actors);

        return deletedActor;
    }

}
module.exports = actorRepository;
