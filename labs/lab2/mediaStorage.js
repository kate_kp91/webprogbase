const multer = require("multer");

const maxSize = 2 * 1024 * 1024;

// jsonStorage.js
class MediaStorage {

    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath = filePath;
         this.storage = multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, "/data/media/");
            },
            filename: (req, file, cb) => {
                console.log(file.originalname);
                cb(null, file.originalname);
            },
        });

         this.uploadFile = multer({
            storage: this.storage,
            limits: { fileSize: maxSize },
        }).single("picture");
    }

    getByName(name) {
        fileStore.get('name', function (err, file) {
            console.log(err, file); // null, { filename: '/a/b/c.png', contentType: 'image/png', length: 512, stream: [Readable] }
        });
    }

    saveWithName(file) {
        fileStore.put(file, function (err) {
            console.log(err); // undefined
        });
    }

    upload(req, res){
        try {
             this.uploadFile(req, res);

            if (req.file == undefined) {
                return res.status(400).send({ message: "Please upload a file!" });
            }

            res.status(200).send({
                message: "Uploaded the file successfully: " + req.file.originalname,
            });
        } catch (err) {
            res.status(500).send({
                message: `Could not upload the file: ${req.file.originalname}. ${err}`,
            });
        }
    };


};

module.exports = MediaStorage;