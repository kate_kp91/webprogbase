const Actor = require('./../models/actor');
const JsonStorage = require('../jsonStorage');


class actorRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getActors() {
        const items = this.storage.readItems();
        var actors = new Array();

        for (const item of items) {

            actors.push(new Actor(item.id, item.name, item.surname, item.age, item.films, item.registeredAt));

        }
        return actors;
    }

    getActorById(id) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id === id) {
                return new Actor(item.id, item.name, item.surname, item.age, item.films, item.registeredAt);
            }
        }
        return null;

    }

    addActor(actorModel) {
        var actors = this.getActors();
        actorModel.id = this.storage.nextId;
        actors.push(actorModel);

        this.storage.incrementNextId();
        this.storage.writeItems(actors);

        return actorModel.id;
    }

    updateActor(actorModel) {
        var actorId = actorModel.id;
        var actors = this.getActors();
        for (var i = 0; i < actors.length; i++) {
            if (actorId === actors[i].id) {
                actors[i] = actorModel;
                break;
            }    
        }

        this.storage.writeItems(actors);
    }

    deleteActor(actorId) {
        var wasActorDeleted = false;
        var actors = this.getActors();
        for (var i = 0; i < actors.length; i++) {
            if (actorId === actors[i].id) {
                actors.splice(i, 1);
                wasActorDeleted = true;
                break;
            }
        }

        this.storage.writeItems(actors);

        return wasActorDeleted;
    }

}
module.exports = actorRepository;
