const UserRepository = require('./repositories/userRepository');
const ActorRepository = require('./repositories/actorRepository');

const userRepository = new UserRepository('./data/users.json');
const actorRepository = new ActorRepository('./data/actors.json');

const readline = require('readline-sync');
const Actor = require('./models/actor');

var exit = false;

while (!exit) {
    const input = readline.question("Enter command:");
   // console.log(input);

    const text = input.toString().trim();
    // example: "get/users"
    const parts = text.split("/");
    const command = parts[0];
    //

    switch (command) {
        case "get" :
            handleGetCommand(parts);
            break;
        case "add":
            handleAddCommand(parts);
            break;

        case "update":
            handleUpdateCommand(parts);
            break;

        case "delete":
            handleDeleteCommand(parts);
            break;

        case "help":
            handleHelpCommand();
            break;

        case "exit":
            exit = true;
            break;
            default : 
            console.log("There is no such command");


    }
}

function handleHelpCommand() {
    console.log("List of commands: ");
    console.log('get/{users | actors} - отримати список всіх сутностей за варіантом');
    console.log('get / { user | actor} / { id } - отримати детальну інформацію про обрану сутність');
    console.log('delete/actor/{ id } - видалити з репозиторія обрану сутність');
    console.log('update /actor/ { name | surname | age | films } - ввести нові дані для обраної сутності і оновити її у репозиторії');
    console.log('add/actor - розпочати заповнення даних нової сутності, яку додати у репозиторій');
    console.log('exit - вихід.');
}

function handleGetCommand(parts) {
    const entity = parts[1];
    if (!checkAvailableCommands(['users', 'user', 'actors', 'actor'], entity)) {
        return;
    }

    switch(entity) {
        case "users":
            var users = userRepository.getUsers();
            for(var i = 0; i < users.length; i++) {
                console.log(`User ${i + 1}: \nLogin: ${users[i].login}\nFullname: "${users[i].fullname}"\nRole: "${users[i].role}"\n`);
            }
            break;

        case "user":
            var entityId = parseInt(parts[2]);

            if (isNaN(entityId)) {
                entityId = readId();
            }

            const user = userRepository.getUserById(entityId);
            if (!user) {
                console.log(`Error: user with id ${entityId} not found.`);
                return;
            }
            console.log(`User: \nLogin: ${user.login}\nFullname: "${user.fullname}"\nRole: "${user.role}"\nRegistredAt: "${user.registeredAt}"\nAvaUrl: "${user.avaUrl}"\nIsEnabled: "${user.isEnabled}"\n`);

            break;

        case "actors":
            var actors = actorRepository.getActors();
            for (var i = 0; i < actors.length; i++) {
                console.log(`Actor ${i + 1}: \nName: ${actors[i].name}\nSurname: "${actors[i].surname}"\n`);
            }
            break;

        case "actor":
            var actorId = parseInt(parts[2]);

            if (isNaN(actorId)) {
                actorId = readId();
            }

            const actor = actorRepository.getActorById(actorId);
            if (!actor) {
                console.log(`Error: actor with id ${actorId} not found.`);
                return;
            }
            console.log(`Actor: \nName: ${actor.name}\nSurname: "${actor.surname}"\nAge: "${actor.age}"\nRegistredAt: "${actor.registeredAt}"\nFilms: "${actor.films}"`);

            break;
    }
}

function handleAddCommand(parts) {
    const entity = parts[1];
    if (!checkAvailableCommands(['actor'], entity)) {
        return;
    }

    if (entity === "actor") {
        const name = readline.question("Enter name:");

        if (name.trim().length == 0) {
            console.log("Name field cant be empty");
            return;
        }

        const surname = readline.question("Enter surname:");
        if (surname.trim().length == 0) {
            console.log("Surname field cant be empty");
            return;
        }

        const age = readline.question("Enter age:");
        if (isNaN(age)) {
            console.log(`Error: ${age} is not a number.`);
            return;
        }
        
        const films = readline.question("Enter films:");
        if (isNaN(films)) {
            console.log(`Error: ${films} is not a number.`);
            return;
        }

        const registeredAt = new Date().toISOString();

        var id = actorRepository.addActor(new Actor(null, name, surname, age, films, registeredAt));
        console.log(`New actor added, id = ${id}`);
    }
}

function handleUpdateCommand(parts) {
    const entity = parts[1];
    if (!checkAvailableCommands(['actor'], entity)) {
        return;
    }

    if (entity === "actor") {
        var entityId = parseInt(parts[2]);
        if (isNaN(entityId)) {
            entityId = readId();
        }

        const actor = actorRepository.getActorById(entityId);
        if (!actor) {

            console.log(`Error: actor with id ${entityId} not found.`);
            return;
        } else {
            console.log(`Actor: \nName: ${actor.name}\nSurname: "${actor.surname}"\nAge: "${actor.age}"\nRegistredAt: "${actor.registeredAt}"\nFilms: "${actor.films}"`);
            const category = parts[3];

            if (!checkAvailableCommands(['name', 'surname', 'age', 'films'], category)) {
                return;
            }

            if (category === "name") {
                const name = readline.question("Enter new name:");
                if (name.trim().length == 0) {
                    console.log("Name field cant be empty");
                    handleUpdateCommand(parts);
                    return;
                }

                actor.name = name;
            }

            if (category === "surname") {
                const surname = readline.question("Enter new surname:");
                if (surname.trim().length == 0) {
                    console.log("Surname field cant be empty");
                    handleUpdateCommand(parts);
                    return;
                }
                actor.surname = surname;
            }

            if (category === "age") {
                const age = readline.question("Enter new age:");
                if (isNaN(age)) {
                    console.log(`Error: ${age} is not a number.`);
                    handleUpdateCommand(parts);
                    return;
                }
                actor.age = age;
            }

            if (category === "films") {
                const films = readline.question("Enter new films:");
                if (isNaN(films)) {
                    console.log(`Error: ${films} is not a number.`);
                    handleUpdateCommand(parts);
                    return;
                }
                actor.films = films;
            }

            actorRepository.updateActor(actor);

            console.log(`New Actor: \nName: ${actor.name}\nSurname: "${actor.surname}"\nAge: "${actor.age}"\nRegistredAt: "${actor.registeredAt}"\nFilms: "${actor.films}"`);
        }
    }
}

function handleDeleteCommand(parts) {
    const entity = parts[1];

    if(!checkAvailableCommands(["actor"], entity)) {
        return;
    }

    if (entity === "actor") {
        var entityId = parseInt(parts[2]);
        if (isNaN(entityId)) {
            entityId = readId();
        }
        const actor = actorRepository.getActorById(entityId);
        if (!actor) {
            console.log(`Error: actor with id ${entityId} not found.`);
            return;
        } else {

            var deleted = actorRepository.deleteActor(actor.id);
            if (deleted) {
                console.log("Actor deleted");
            } else {
                console.log("Actor not deleted");
            }
        }
    }
}

function checkAvailableCommands(commands, command) {

    if (commands.includes(command)) {
        return true;
    } else {
        console.log(`There is no such command ${command}`);
        commands.length == 1 ? console.log(`Should be "${commands}"`) : console.log(`Should be one of "${commands}"`);
        return false;
    }
}

function readId() {
    var intVal = parseInt(readline.question("Enter id of entity:"));
    if (isNaN(intVal)) {
        return readId();
    } else {
        return intVal;
    }
}


