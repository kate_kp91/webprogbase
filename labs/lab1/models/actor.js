class Actor {

    constructor(id, name, surname, age, films, registeredAt) {
        this.id = id;  // number
        this.name = name;  // string
        this.surname = surname;  // string
        this.age = age;
        this.films = films;
        this.registeredAt = registeredAt;
        }
};

module.exports = Actor;
