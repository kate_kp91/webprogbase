const express = require("express");
const app = express();
const methodOverride = require('method-override')

const body_parser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

const morgan = require("morgan");

const mongoose = require("mongoose");
require("dotenv").config();

const routesIndex = require("./routes/index");
const routesUsers = require("./routes/users");
const routesActors = require("./routes/actors");
const routesFilms = require("./routes/films");
const routesApi = require("./routes/api");

app.locals.moment = require('moment');
app.use(morgan("dev"));
app.set("view engine", "jade");

app.use(body_parser.urlencoded({ extended: true }));
app.use(body_parser.json());
app.use(busboyBodyParser());

const dbUrl = process.env.DATABASE_URL;
const connectOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

const port = process.env.PORT || 3000;


app.use(methodOverride('_method'))

app.use("/", routesIndex);
app.use("/users", routesUsers);
app.use("/actors", routesActors);
app.use("/films", routesFilms);
app.use("/api", routesApi);
app.use(express.static(__dirname + "/public"));
app.use("/data/media", express.static(__dirname + "/data/media"));

const expressSwaggerGenerator = require("express-swagger-generator");
const expressSwagger = expressSwaggerGenerator(app);

const options = {
  swaggerDefinition: {
    info: {
      description: "Creating HTTP API",
      title: "Lab 3",
      version: "1.0.0",
    },
    host: "localhost" + ":" + port,
    produces: ["application/json"],
  },
  basedir: __dirname,
  files: ["./routes/**/*.js", "./models/**/*.js"],
};
expressSwagger(options);

//app.listen(port, () => console.log("Gator app listening on port !" + port));

app.listen(port, async () => {
  try {
    console.log(`Server ready`);
    const client = await mongoose.connect(dbUrl, connectOptions);
    console.log('Mongo database connected');
  } catch (error) {
    console.log(error);
  }
});
