const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');


exports.getUsers = async function (req, res) {
  var users = await userRepository.getUsers(req.query.pageNumber, req.query.perPage);
  if (users.length > 0) {
    res.status(200).send({ users, message: 'Success'});
  } else {
    res.status(200).send({ users, message: 'There is no users!'});
  }
};

exports.getUser = async function (req, res) {
  var user = await userRepository.getUserById(req.params.id);

  if (user) {
    res.status(200).send({ user, message: 'Success' });
  } else {
    res.status(404).send({ user: null, message: 'User not found id:' + req.params.id });
  }
};

exports.getUsersPage = async function (req, res) {
  var users = await userRepository.getUsers(req.query.pageNumber, req.query.perPage);
  res.render("pages/users", { users: users , title: 'Actors'});
};


exports.getUserPage = async function (req, res) {
  var user = await userRepository.getUserById(req.params.id);
  if (user) {
    res.render("pages/user", { user: user, title: 'Actor: ' + user.fullname });
  } else {
    res.render("error", { message: 'User not found id:' + req.params.id, error: {status: 404, stack:''} });
  }
};
