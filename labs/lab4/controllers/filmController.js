const Film = require("../models/film");
const Repository = require("../repositories/filmRepository");
const repository = new Repository();
const perPage = 5;

exports.getFilmsPage = async function (req, res) {
  try {
    const page = req.query.page == undefined ? 1 : req.query.page;
    const { films, total } = await repository.getFilms(
        page,
        req.query.search,
        req.params.actor_id
    );
    res.render("pages/films", {
      films: films,
      title: "Films",
      search: req.query.search ? req.query.search : "",
      page: {current: page, total: Math.ceil(total / perPage)},
      actor_id: req.params.actor_id,
      user_id: req.params.user_id
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ films: null, message: 'Server error.' });
  }

};

exports.getFilmPage = async function (req, res) {
  try {
    var film = await repository.getFilmById(req.params.id);
    if (film) {
      res.render("pages/film", { film: film, title: "film: " + film.name, user_id : req.params.user_id });
  } else {
    res.render("error", {
      message: "Film not found id:" + req.params.id,
      error: { status: 404, stack: "" },
    });
  }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ actors: null, message: 'Server error.' });
  }
  
};

exports.getFilmNewPage = function (req, res) {
  console.log(req.params)
  res.render("pages/filmCreate", { user_id: req.params.user_id, actor_id: req.params.actor_id });
};

exports.deleteFilmRedirect = async function (req, res) {
  try {
    var film = await repository.deleteFilm(req.params.id);
    if (film) {
      res.redirect(301, "/users/" + req.params.user_id + "/actors/" + req.params.actor_id + "/films");
    } else {
      res.status(404).send({
        film: null,
        message: "Not deleted. Film not found id:" + req.params.id,
      });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ film: null, message: 'Server error.' });
  }
};

exports.createFilmRedirect = async function (req, res) {

  req.body.actor_id = req.params.actor_id;

  const filmId =  await repository.addFilm(req.body );
  if (filmId) {
    res.redirect(301, "films/" + filmId);
    } else {
      res.render("pages/filmCreate", {
        film: null,
        message: "FilmId not created"
      });

      //res.status(code).send({ actor, message: status });
    
  }
};



