const Actor = require("../models/actor");
const Repository = require("../repositories/actorRepository");
const repository = new Repository();
const formidable = require('formidable')
const perPage = 5;

const cloudinary = require('cloudinary');
const e = require("express");

require("dotenv").config();
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});


exports.getActors = async function (req, res) {
  try {
    const { actors } = await repository.getActors(
      req.query.page,
      req.query.search
    );

    if (actors.length > 0) {
      res.status(200).send({ actors, message: "Success" });
    } else {
      res.status(200).send({ actors, message: "There is no actors!" });
    }
  } catch (error) {
    console.log(err.message);
    res.status(500).send({ actors: null, message: 'Server error.' });
  }
};

exports.getActor = async function (req, res) {
  try {
    var id = req.params.id;
    var actor = await repository.getActorById(id);
    if (actor) {
      res.status(200).send({ actor, message: "Success" });
    } else {
      res.status(404).send({ actor: null, message: "Actor not found" });
    }
  } catch (error) {
    console.log(err.message);
    res.status(500).send({ actors: null, message: 'Server error.' });
  }
};

exports.getActorsPage = async function (req, res) {
  try {
    const page = req.query.page == undefined ? 1 : req.query.page;
    console.log(req.params)
    const { actors, total } = await repository.getActors(
        page,
        req.query.search,
        req.params.user_id
    );
    res.render("pages/actors", {
      actors: actors,
      title: "Actors",
      search: req.query.search ? req.query.search : "",
      page: {current: page, total: Math.ceil(total / perPage)},
      user_id: req.params.user_id
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ actors: null, message: 'Server error.' });
  }

};

exports.getActorPage = async function (req, res) {
  try {
    var actor = await repository.getActorById(req.params.id);
  if (actor) {
    res.render("pages/actor", { actor: actor, title: "Actor: " + actor.name, user_id: req.params.user_id });
  } else {
    res.render("error", {
      message: "Actor not found id:" + req.params.id,
      error: { status: 404, stack: "" },
    });
  }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ actors: null, message: 'Server error.' });
  }
  
};

exports.getActorNewPage = function (req, res) {
  console.log(req.params)
  res.render("pages/actorCreate", {user_id: req.params.user_id});
};

exports.createActor = async function (req, res) {
  req.body.user_id = req.params.user_id;
  repository.addActor(req, (code, status, actor) => {
    res.status(code).send({ actor, message: status });
  });
};

exports.updateActor = async function (req, res) {
  /*var actor = req.body;
  var updatedActor = repository.updateActor(new Actor(actor));
  if (updatedActor) {
    res.status(200).send({ updatedActor, message: "Update is Successfull" });
  } else {
    res
        .status(404)
        .send({ updatedActor: null, message: "Not updated. Actor not found" });
  }*/
};

exports.deleteActor = async function (req, res) {
  try {
    var actor = await repository.deleteActor(req.params.id);
    if (actor) {
      res.status(200).send({ actor, message: "Delet is successfull" });
    } else {
      res.status(404).send({
        actor: null,
        message: "Not deleted. Actor not found id:" + req.params.id,
      });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ actor: null, message: 'Server error.' });
  }
  
};

exports.deleteActorRedirect = async function (req, res) {
  try {
    var actor = await repository.deleteActor(req.params.id);
    if (actor) {
      res.redirect(301, "/users/" + req.params.user_id + "/actors/")
    } else {
      res.status(404).send({
        actor: null,
        message: "Not deleted. Actor not found id:" + req.params.id,
      });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ actor: null, message: 'Server error.' });
  }
};

exports.createActorRedirect = async function (req, res) {
  const result = await uploadRaw(req.files['file'].data);
  console.log(req)
  req.body.avatar = result.url;
  req.body.user_id = req.params.user_id;

 const actorId =  await repository.addActor(req.body );
    if (actorId) {
      res.redirect(301, "/users/"+req.body.user_id + "/actors/" + actorId);
    } else {
      res.render("pages/actorCreate", {
        actor: null,
        message: "Actor not created"
      });

      //res.status(code).send({ actor, message: status });
    
  }
};

uploadRaw = async function(buffer) {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader
      .upload_stream({ resource_type: 'raw' },
        (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        })
      .end(buffer);
  });
}


