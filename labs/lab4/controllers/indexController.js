exports.getIndex = function (req, res) {
  res.render("index", { title: 'Home' });
};

exports.getAbout = function (req, res) {
  res.render("about", { title: 'About' });
};