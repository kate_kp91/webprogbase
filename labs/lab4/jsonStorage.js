const fs = require('fs');

// jsonStorage.js
class JsonStorage {

    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.nextId;
    }

    incrementNextId() {
        var id = this.nextId;
        id = id + 1;
       
        var json = {
            "nextId": id,
            "items": this.readItems()
        };

        const jsonText = JSON.stringify(json, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.items;
    }

    writeItems(items) {
        var json = {"nextId": this.nextId,
                    "items" : items };

        const jsonText = JSON.stringify(json, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }
};

module.exports = JsonStorage;