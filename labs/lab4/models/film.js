
const mongoose = require('mongoose');
/**
 * @typedef Film
 * @property {integer} id
 * @property {string} name.required - name of  film
 * @property {string} rating.required - rating of film
 * @property {string} actor_id.required - actor_id
 */

const FilmSchema = new mongoose.Schema({
  name: { type: String, required: true },
  rating: { type: String, required: true },
  actor_id: { type: mongoose.Types.ObjectId, ref: 'Actor', required: true }
}, { collection: 'Films' });


module.exports = mongoose.model('Films', FilmSchema);
