
const mongoose = require('mongoose');
/**
 * @typedef Actor
 * @property {integer} id
 * @property {string} name.required - name of  actor
 * @property {string} surname.required - surname of actor
 * @property {string} avatar - avatar of actor
 * @property {integer} age.required - age of actor
 * @property {string} registeredAt - time when actor registered
 */

const ActorSchema = new mongoose.Schema({
  name: { type: String, required: true },
  surname: { type: String, required: true },
  registeredAt: { type: String, default: new Date().toISOString() },
  avatar: { type: String },
  age: { type: Number, required: true},
  user_id: { type: mongoose.Types.ObjectId, ref: 'Users', required: true }
}, { collection: 'Actors' });


module.exports = mongoose.model('Actors', ActorSchema);
