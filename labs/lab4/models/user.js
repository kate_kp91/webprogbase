// models/user.js

const mongoose = require('mongoose');

/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - full name of user
 * @property {integer} role - role of user
 * @property {string} registeredAt - time when user registered
 * @property {string} avaUrl - url address of user avatart
 * @property {boolean} isEnabled - flag that determines is user enabled or diactivated
 * @property {boolean} country 
 * @property {boolean} email 
 * @property {boolean} phone 
 */

const UserSchema = new mongoose.Schema({
  login: { type: String, required: true },
  fullname: { type: String, required: true },
  role: { type: Number, required: true },
  registeredAt: { type: String, default: new Date().toISOString() },
  avaUrl: { type: String },
  isEnabled: { type: Boolean },
  country: { type: String },
  email: { type: String },
  phone: { type: String }
}, { collection: 'Users' });

const User = mongoose.model('Users', UserSchema);

module.exports = User;

