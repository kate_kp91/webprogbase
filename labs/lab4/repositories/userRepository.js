const User = require('./../models/user');
const JsonStorage = require('../jsonStorage');

const perPage = 5;


class UserRepository {

    async getUsers(page, per_page) {
        per_page =  per_page > 0 ? per_page : perPage;

        const items = await User.find();
        var users = new Array();
        var itemsCount = items.length;
        
        var end = page > 0 && per_page > 0 ? page * per_page : itemsCount;
        var start = page > 0 && per_page > 0 ? end - per_page : 0;
        
        for (var i = start; (i < end && i < itemsCount); i++) {
            
            var item = items[i];
            
            users.push(new User(item));
           
        }

        console.log(users)
        return users;
    }

    async getUserById(id) {
        const user = await User.findById(id);
        return user;

    }

    addUser(userModel) {
        throw new Error("Not implemented");
    }

    updateUser(userModel) {
        throw new Error("Not implemented");
    }

    deleteUser(userId) {
        throw new Error("Not implemented");
    }

}
module.exports = UserRepository;


module.exports = UserRepository;
