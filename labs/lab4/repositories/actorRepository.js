const Actor = require("./../models/actor");
const Film = require("./../models/film");
const JsonStorage = require("../jsonStorage");
const formidable = require('formidable')

const perPage = 5;
const dirPath = 'data/media/';
const contain = (s1, s2) => {
  s1 = s1.toLocaleLowerCase();
  s2 = s2.toLocaleLowerCase();

  return s1.indexOf(s2) > -1;
};
class actorRepository {

  async getActors(page, search, user_id) {
    console.log(user_id)
    let items = await Actor.find({ user_id: user_id });
    var actors = new Array();
    if (search && search.trim()) {
      items = items.filter(
          (item) => contain(item.name, search) || contain(item.surname, search)
      );
    }

    var end = page > 0 ? page * perPage : items.length;
    var start = page > 0 ? end - perPage : 0;

    for (var i = start; i < end && i < items.length; i++) {
      var item = items[i];
      actors.push(new Actor(item));
    }
    return { actors, total: items.length };
  }

  async getActorById(id) {
    const items = await Actor.findById(id)
    
    return items;
  }

  /*
  addFile(req, next, mediaP) {
    var mediaPath = "";
    console.log("adding file")
    formidable.IncomingForm()
      .parse(req)
      .on("fileBegin", (name, file) => {
        file.path = __dirname + "/../" + dirPath + file.name;
        console.log(" file begin" + file.path)

      })
      .on("file", (name, file) => {
        console.log(" on file" + file.path)
        mediaPath = dirPath + file.name;
        mediaP(mediaPath)
      })
      .on("aborted", () => {
        console.error("Request aborted by the user");
        mediaP("")
        next(400, "Request aborted by the user");
      })
      .on("error", (err) => {
        console.error("Error", err);
        mediaP("")
        next(400, err);
        throw err;
      });
  }
  */

  async addActor(actorModel) {
    console.log(actorModel)
    const actor = new Actor(actorModel);
    await Actor.insertMany(actor);
    return actor._id
  }

 /* async updateActor(req) {
    var actorModel = req.body;

    this.addFile(req, null,(mediaPath)=> {
      var actorId = actorModel.id;
      var actors = this.getActors().actors;
      var updated = false;
      for (var i = 0; i < actors.length; i++) {
        if (actorId == actors[i].id) {
          if (actorModel.name) actors[i].name = actorModel.name;
          if (actorModel.surname) actors[i].surname = actorModel.surname;
          if (actorModel.avatar) actors[i].avatar = mediaPath;
          if (actorModel.age != null) actors[i].age = actorModel.age;
          if (actorModel.films != null) actors[i].films = actorModel.films;
          if (actorModel.registeredAt)
            actors[i].registeredAt = actorModel.registeredAt;
          updated = true;
          break;
        }
      }

      if (updated) {
        this.storage.writeItems(actors);
        return this.getActorById(actorId);
      } else {
        return null;
      }
    });
  }
  */

  async  deleteActor(actorId) {
   
    var actors = await Actor.findById(actorId);
    await Film.deleteMany({actor_id:actorId})
    await Actor.deleteOne({ _id: actorId });

    return actors;
  }
}
module.exports = actorRepository;
