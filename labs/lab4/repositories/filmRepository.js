const Film = require("../models/film");

const perPage = 5;

const contain = (s1, s2) => {
  s1 = s1.toLocaleLowerCase();
  s2 = s2.toLocaleLowerCase();

  return s1.indexOf(s2) > -1;
};
class filmRepository {

  async getFilms(page, search, actor_id) {
    let items = await Film.find({actor_id : actor_id});
    var films = new Array();
    if (search && search.trim()) {
      items = items.filter(
          (item) => contain(item.name, search) || contain(item.rating, search)
      );
    }

    var end = page > 0 ? page * perPage : items.length;
    var start = page > 0 ? end - perPage : 0;

    for (var i = start; i < end && i < items.length; i++) {
      var item = items[i];
      films.push(new Film(item));
    }
    return { films, total: items.length };
  }

  async getFilmById(id) {
    const items = await Film.findById(id)
    
    return items;
  }

  async addFilm(filmModel) {
    console.log(filmModel)
    const film = new Film(filmModel);
    await Film.insertMany(film);
    return film._id
  }

  async deleteFilm(filmId) {
   
    var films = await Film.findById(filmId);
    await Film.deleteOne({ _id: filmId });

    return films;
  }
}
module.exports = filmRepository;
