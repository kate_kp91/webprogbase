var express = require('express');
var router = express.Router();
var controller = require('../controllers/filmController');
var router = express.Router({ mergeParams: true });


router.get('/', controller.getFilmsPage);

router.get('/new', controller.getFilmNewPage);

router.get('/:id', controller.getFilmPage);

router.delete('/:id', controller.deleteFilmRedirect);

router.post('/', controller.createFilmRedirect);

module.exports = router;
