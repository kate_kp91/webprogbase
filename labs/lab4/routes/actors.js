var express = require('express');
var router = express.Router({ mergeParams: true });
var controller = require('../controllers/actorController');
const routesFilms = require("../routes/films");

router.get('/', controller.getActorsPage);

router.get('/new', controller.getActorNewPage);

router.get('/:id', controller.getActorPage);

router.delete('/:id', controller.deleteActorRedirect);

router.post('/', controller.createActorRedirect);

router.use('/:actor_id/films', routesFilms);

module.exports = router;
