var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');



/**
 * GET /api/users     -> [{User}, {User}]. 
 * getting list of users
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} pageNumber.query - displayed page - eg:1
 * @param {integer} perPage.query - displayed count of users per page - eg:10
 * @returns {Array.<User>} 200 - All users
 */
router.get('/api/users', userController.getUsers);

/**
 * GET /api/users/:id -> {User}.
 * getting user by id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get('/api/users/:id', userController.getUser);


/**
 * GET /users     -> [{User}, {User}]. 
 * getting list of users
 * @route GET /users
 * @group Users - user operations
 * @param {integer} pageNumber.query - displayed page - eg:1
 * @param {integer} perPage.query - displayed count of users per page - eg:10
 * @returns {Array.<User>} 200 - All users
 */
router.get('/', userController.getUsersPage);

/**
 * GET /users/:id -> {User}.
 * getting user by id
 * @route GET /users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get('/:id', userController.getUserPage);

//router.use('/:id/stations', metroRouter);

const actorRouter = require('./actors');

router.use('/:user_id/actors', actorRouter);

module.exports = router;
