var express = require('express');
var router = express.Router();
var indexController = require('../controllers/indexController');

/**
 * GET / 
 * Displaying index page
 * @route GET /
 */
router.get('/', indexController.getIndex);

/**
 * GET / 
 * Displaying about page
 * @route GET /about
 */
router.get('/about', indexController.getAbout);


module.exports = router;