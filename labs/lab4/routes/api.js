var express = require('express');
var router = express.Router();
var controller = require('../controllers/actorController');
var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()


/**
 * GET /actors     -> [{Actor}, {Actor}].
 * getting list of actors
 * @route GET /actors
 * @group Actors - actor operations
 * @param {integer} pageNumber.query - displayed page - eg:1
 * @param {integer} perPage.query - displayed count of actors per page - eg:10
 * @returns {Array.<Actor>} 200 - Page with actors
 */
router.get('/actors', controller.getActors);

/**
 * GET /actors/:id -> {Actor}.
 * getting actor by id
 * @route GET /actors/{id}
 * @group Actors - Actor operations
 * @param {integer} id.path.required - id of the Actor - eg: 1
 * @returns {Actor.model} 200 - Actor object
 * @returns {Error} 404 - Actor not found
 */
router.get('/actors/:id', controller.getActor);

//POST   /{entities}     -> {Entity}               // return added entity */
/**
 * Creating new actor
 * @route POST /actors
 * @group Actors - actor operations
 * @param {Actor.model} id.body.required - new Actor object
 * @returns {Actor.model} 201 - added Actor object
 */
router.post('/actors', controller.createActor);

//PUT    /{entities}     -> {Entity}               // return changed entity
/**
 * Updating actor info
 * @route PUT /actors
 * @group Actors - actor operations
 * @param {Actor.model} id.body.required - new Actor object
 * @returns {Actor.model} 200 - changed Actor object
 * @returns {Error} 404 - Actor not found
 */
router.put('/actors', jsonParser, controller.updateActor);

//DELETE /{entities}/:id -> {Entity}               // return removed entity
/**
 * Deleting actor by id
 * @route DELETE /actors/{id}
 * @group Actors - actor operations
 * @param {integer} id.path.required - id of the Actor - eg: 1
 * @returns {Actor.model} 200 - deleted Actor object
 * @returns {Error} 404 - Actor not found
 */
router.delete('/actors/:id', controller.deleteActor);


module.exports = router;
